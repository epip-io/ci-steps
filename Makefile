ifeq ($(CI_STEPS_FILE),)
  export CI_STEPS_FILE := $(abspath $(lastword $(MAKEFILE_LIST)))

endif
export CI_STEPS_PATH := $(abspath $(dir $(CI_STEPS_FILE)))
export CI_STEPS_PATH_LIB := $(abspath $(dir $(CI_STEPS_FILE)))/lib
export CI_STEPS_PATH_CACHE ?= $(CURDIR)/.ci-cache

ifneq (,$(wildcard $(CI_STEPS_PATH_CACHE)/*.mk))
  include $(CI_STEPS_PATH_CACHE)/*.mk
endif

CI_STEPS_LIB_FILTERS := $(wildcard $(CI_STEPS_PATH_LIB)/core/*)

include $(CI_STEPS_PATH)/lib/core/define.mk

ifeq ($(DRONE),true)
  include $(CI_STEPS_PATH)/lib/core/drone.mk

endif

include $(CI_STEPS_PATH)/lib/core/init.mk
include $(CI_STEPS_PATH)/lib/core/Makefile

# TODO: include all non-core plugin components
-include $(filter-out $(CI_STEPS_LIB_FILTERS),$(wildcard $(CI_STEPS_PATH_LIB)/*/define.mk))

ifeq ($(DRONE),true)
  -include $(filter-out $(CI_STEPS_LIB_FILTERS),$(wildcard $(CI_STEPS_PATH_LIB)/*/drone.mk))

endif

-include $(filter-out $(CI_STEPS_LIB_FILTERS),$(wildcard $(CI_STEPS_PATH_LIB)/*/init.mk))
-include $(filter-out $(CI_STEPS_LIB_FILTERS),$(wildcard $(CI_STEPS_PATH_LIB)/*/Makefile))

ifndef TRANSLATE_COLON_NOTATION
%:
	@$(SELF) -s $(subst :,/,$@) TRANSLATE_COLON_NOTATION=false
endif