## [Required] What make steps do you want to execute?
ifneq ($(PLUGIN_STEPS),)
  export CI_STEPS = $(subst $(CI_STEPS_COMMA),$(CI_STEPS_SPACE),$(PLUGIN_STEPS))
endif

## (Optional) Additional steps to initialize?
ifneq ($(PLUGIN_ADDITIONAL),)
  export CI_STEPS_ADDITIONAL = $(subst $(CI_STEPS_COMMA),$(CI_STEPS_SPACE),$(PLUGIN_ADDITIONAL))
endif

## (Optional) What is the step name, i.e. clone, commit, etc?
export CI_STEPS_STEP_NAME ?= $(DRONE_STEP_NAME)
