## Lint all makefiles
make/lint:
	@LINT=true \
	 find . -type f -name '*Makefile*' \
	  		-type f '!' -name '*.example' \
	  		-type f '!' -path './bin/*' -exec \
			 /bin/sh -c 'echo "==> {}">/dev/stderr; make --include-dir=$(CI_STEPS_PATH)/lib/ --just-print --dry-run --recon --no-print-directory --quiet --silent -f {}' \; > /dev/null
	@$(SELF) bash/lint
