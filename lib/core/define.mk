# Ensures that a variable is defined
define assert-set
  @[ -n "$($1)" ] || (echo "$(1) not defined in $(@)"; exit 1)
endef

# Ensures that a variable is undefined
define assert-unset
  @[ -z "$($1)" ] || (echo "$(1) should not be defined in $(@)"; exit 1)
endef

export CI_STEPS_COMMA := ,
export CI_STEPS_SPACE :=
export CI_STEPS_SPACE +=