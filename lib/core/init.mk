export CI_STEPS_PATH := $(abspath $(dir $(CI_STEPS_FILE)))

ifeq ($(CURDIR),$(realpath $(CI_STEPS_PATH)))
  export CI_STEPS_HELP_TARGET ?= help/all

else
  export CI_STEPS_HELP_TARGET ?= help/short

endif

export CI_STEPS ?= $(CI_STEPS_HELP_TARGET)

default:: $(CI_STEPS)
	@echo "executed: $(CI_STEPS)"

ifeq (true,$(CI))
cache:: 
	@$(SELF) -s cache/env \
		CI_STEPS_CACHE="core" \
		CI_STEPS_CACHE_ENV="CI_" \
		CI_STEPS_CACHE_EXCLUDE="_SELF_|CI_STEPS=|CI_STEPS_ADDITIONAL=|EMAIL" \
		CI_STEPS_CACHE_EXT="mk" \
		CI_STEPS_CACHE_PREFIX="export " \
		CI_STEPS_CACHE_SEP=" := "

init::
	@$(SELF) -s cache/env/clear CI_STEPS_CACHE="core"

else
init::
	@exit 0

endif
