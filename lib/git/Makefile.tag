## Add version tag to commit
git/tag/version:
	@$(call assert-set,GIT_RELEASE)
	@$(call assert-set,GIT_COMMIT_TAG)
	@RELEASE_TYPE=$(GIT_RELEASE) \
		$(GIT) tag -a -m "$${RELEASE_TYPE^} Version $(GIT_COMMIT_TAG:$(GIT_TAG_VERSION_PREFIX)%=%)" $(GIT_COMMIT_TAG)
