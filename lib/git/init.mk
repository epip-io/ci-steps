export GIT := $(shell which git)

ifneq ($(GIT),)
  ifeq (,$(findstring git,$(CI_STEPS_ADDITIONAL)))
    ifeq ($(DEBUG),true)
      $(warning enabling git steps)
    endif
	  export CI_STEPS_ADDITIONAL += git
  endif

  ifneq (,$(findstring git,$(CI_STEPS_ADDITIONAL)))
    ifeq ($(GIT_FETCH_FLAGS),)
      export GIT_FETCH_FLAGS = --tags

    else
      ifeq (,$(filter --tags,$(GIT_FETCH_FLAGS)))
        export GIT_FETCH_FLAGS += --tags

      endif
    endif

    ifeq (true,$(shell $(GIT) rev-parse --is-inside-work-tree 2>/dev/null))
      export GIT_URL := $(shell $(GIT) remote get-url origin 2>/dev/null)
      export GIT_ORG := $(shell basename $$(dirname "$(GIT_URL)" | cut -d: -f2))
      export GIT_REPO := $(GIT_ORG)/$(shell basename "$(GIT_URL)" .git)
      
      export GIT_COMMIT_AUTHOR_NAME ?= $(shell $(GIT) log -1 --pretty=format:'%an' 2>/dev/null)
      export GIT_COMMIT_AUTHOR_EMAIL ?= $(shell $(GIT) log -1 --pretty=format:'%ae' 2>/dev/null)
      export GIT_COMMIT_BRANCH ?= $(shell $(GIT) rev-parse --abbrev-ref --verify HEAD 2>/dev/null)
      export GIT_COMMIT_MESSAGE := $(shell $(GIT) show -s --format='%s\n\n%b' 2>/dev/null)
      export GIT_COMMIT_MESSAGE_SHORT := $(shell $(GIT) show -s --format=%s 2>/dev/null)
      export GIT_COMMIT_SHA ?= $(shell $(GIT) rev-parse --verify HEAD 2>/dev/null)
      export GIT_COMMIT_SHA_SHORT := $(shell $(GIT) rev-parse --verify --short $(GIT_COMMIT_SHA) 2>/dev/null)
      export GIT_COMMIT_TIMESTAMP := $(shell $(GIT) log -1 --format=%ct 2>/dev/null)
      export GIT_COMMIT_URL := $(shell $(GIT) config --get remote.origin.url 2>/dev/null | sed 's/\.git$$//g' | sed 's/git@\(.*\):/https:\/\/\1\//g')/commit/$(GIT_COMMIT_SHA_SHORT)
      export GIT_COMMIT_VERSION = $(shell $(GIT) describe --match $(GIT_TAG_VERSION_PREFIX)[0-9]*.[0-9]*.[0-9]* --tags --abbrev=0 2>/dev/null)
      export GIT_AUTHOR_NAME ?= $(GIT_COMMIT_AUTHOR_NAME)
      export GIT_AUTHOR_EMAIL ?= $(GIT_COMMIT_AUTHOR_EMAIL)
      export GIT_COMMITTER_NAME ?= $(GIT_COMMIT_AUTHOR_NAME)
      export GIT_COMMITTER_EMAIL ?= $(GIT_COMMIT_AUTHOR_EMAIL)

      export GIT_RELEASE_BRANCH ?= master
      export GIT_RELEASE_VERSION = $(shell $(GIT) describe --match $(GIT_TAG_VERSION_PREFIX)[0-9]*.[0-9]*.[0-9]* --tags --abbrev=0 origin/$(GIT_RELEASE_BRANCH) 2>/dev/null)
      export GIT_RELEASE_VERSIONS = $(shell $(GIT) ls-remote -q --refs --tags --sort="v:refname"  2>/dev/null | awk '{ print $$2 }' | sed 's~^refs/tags/~~g' | tr "\n" " ")
      
      export GIT_TAG_VERSION_PREFIX ?= v

      ifeq ($(GIT_COMMIT_BRANCH),)
        export GIT_CLONE_TYPE = tag

        ifeq ($(GIT_COMMIT_VERSION),$(GIT_RELEASE_VERSION))
          export GIT_RELEASE = production

        else
          export GIT_RELEASE = development

        endif
      else
        export GIT_CLONE_TYPE = $(GIT_BUILD_EVENT)
        GIT_COMMIT_FILTER = $(subst %,,$(GIT_MAJOR_BRANCH))
        ifneq (,$(filter $(GIT_MAJOR_BRANCH),$(GIT_COMMIT_BRANCH))$(findstring $(GIT_COMMIT_FILTER),$(GIT_COMMIT_MESSAGE_SHORT)))
          export GIT_RELEASE_TYPE = major

        endif

        GIT_COMMIT_FILTER = $(subst %,,$(GIT_FEATURE_BRANCH))
        ifneq (,$(filter $(GIT_FEATURE_BRANCH),$(GIT_COMMIT_BRANCH))$(findstring $(GIT_COMMIT_FILTER),$(GIT_COMMIT_MESSAGE_SHORT)))
          export GIT_RELEASE_TYPE = feature

        endif

        GIT_COMMIT_FILTER = $(subst %,,$(GIT_HOTFIX_BRANCH))
        ifneq (,$(filter $(GIT_HOTFIX_BRANCH),$(GIT_COMMIT_BRANCH))$(findstring $(GIT_COMMIT_FILTER),$(GIT_COMMIT_MESSAGE_SHORT)))
          export GIT_RELEASE_TYPE = hotfix
        endif

        ifeq ($(GIT_COMMIT_BRANCH),$(GIT_RELEASE_BRANCH))
          export GIT_RELEASE = production

        else
          export GIT_RELEASE = development

        endif

        ifneq (,$(SEMVER_GIT_TAG_VERSION))
          export GIT_COMMIT_TAG = $(SEMVER_GIT_TAG_VERSION)

        endif
      endif
    endif
  endif

else
  ifneq (,$(findstring git,$(CI_STEPS_ADDITIONAL)))

## Initialize git steps
init::
ifneq ($(OS),linux)
	@echo "git: unable to initialize on $(OS)"
	@exit 1
endif

ifeq (,$(findstring $(OS_DISTRO),$(OS_DISTROS)))
	@echo "git: unable to initialize on $(OS_DISTRO) $(OS)"
	@exit 1
endif

	$(PKGADD) git git-lfs openssh-client

  endif
endif

ifeq (truegit,$(CI)$(findstring git,$(CI_STEPS_ADDITIONAL)))
cache:: 
	@$(SELF) -s cache/env \
    CI_STEPS_CACHE="git" \
    CI_STEPS_CACHE_ENV="GIT_" \
    CI_STEPS_CACHE_EXCLUDE="_SELF_|EMAIL|_NETRC_|PASSWORD|USERNAME|SSH_KEY" \
		CI_STEPS_CACHE_EXT="mk" \
		CI_STEPS_CACHE_PREFIX="export " \
		CI_STEPS_CACHE_SEP=" := "

init:: git/config
	@$(SELF) -s cache/env/clear CI_STEPS_CACHE="git"
endif
