ifneq (,$(filter git,$(CI_STEPS_ADDITIONAL)))
  export GIT_SSH_KEY := $(PLUGIN_SSH_KEY)

  ## (Optional) What is the repository's remote URL?
  export GIT_REMOTE_URL ?= $(PLUGIN_REMOTE_URL)
  ifeq ($(GIT_REMOTE_URL),)
    export GIT_REMOTE_URL = $(DRONE_REMOTE_URL)
  endif

  ## (Optional) What is the branch to work with?
  export GIT_COMMIT_BRANCH ?= $(PLUGIN_COMMIT_BRANCH)
  ifeq ($(GIT_COMMIT_BRANCH),)
    export GIT_COMMIT_BRANCH = $(DRONE_BRANCH)
  endif

  ## (Optional) What is the tag to work with?
  export GIT_COMMIT_TAG ?= $(PLUGIN_COMMIT_TAG)
  ifeq ($(GIT_COMMIT_TAG),)
    export GIT_COMMIT_TAG = $(DRONE_TAG)
  endif

  ## (Optional) What is the commit ref to work with?
  export GIT_COMMIT_REF ?= $(PLUGIN_COMMIT_REF)
  ifeq ($(GIT_COMMIT_REF),)
    export GIT_COMMIT_REF = $(DRONE_COMMIT_REF)
  endif

  ## (Optional) What is the commit sha to work with?
  export GIT_COMMIT_SHA ?= $(PLUGIN_COMMIT_SHA)
  ifeq ($(GIT_COMMIT_SHA),)
    export GIT_COMMIT_SHA = $(DRONE_COMMIT_SHA)
  endif

  ## (Optional) What is build event?
  export GIT_BUILD_EVENT ?= $(PLUGIN_BUILD_EVENT)
  ifeq ($(GIT_BUILD_EVENT),)
    export GIT_BUILD_EVENT = $(DRONE_BUILD_EVENT)
  endif

  ## (Optional) What is the release branch name? (Default: master)
  export GIT_RELEASE_BRANCH ?= $(PLUGIN_RELEASE_BRANCH)
  ifeq ($(GIT_RELEASE_BRANCH),)
    export GIT_RELEASE_BRANCH = master
  endif

  ## (Optional) What is the feature branch pattern? (Default: release/%)
  export GIT_MAJOR_BRANCH ?= $(PLUGIN_MAJOR_BRANCH)
  ifeq ($(GIT_MAJOR_BRANCH),)
    export GIT_MAJOR_BRANCH = release/%
  endif

  ## (Optional) What is the feature branch pattern? (Default: feature/%)
  export GIT_FEATURE_BRANCH ?= $(PLUGIN_FEATURE_BRANCH)
  ifeq ($(GIT_FEATURE_BRANCH),)
    export GIT_FEATURE_BRANCH = feature/%
  endif

  ## (Optional) What is the hotfix branch pattern? (Default: hotfix/%
  export GIT_HOTFIX_BRANCH ?= $(PLUGIN_HOTFIX_BRANCH)
  ifeq ($(GIT_HOTFIX_BRANCH),)
    export GIT_HOTFIX_BRANCH = hotfix/%
  endif

  export GIT_COMMIT_AUTHOR_NAME ?= $(DRONE_COMMIT_AUTHOR_NAME)
  export GIT_COMMIT_AUTHOR_EMAIL ?= $(DRONE_COMMIT_AUTHOR_EMAIL)

  ifneq ($(PLUGIN_DEPTH),)
    export GIT_FETCH_FLAGS = --depth=$(PLUGIN_DEPTH)
  endif

  export GIT_NETRC_MACHINE := $(DRONE_NETRC_MACHINE)
  export GIT_NETRC_USERNAME := $(DRONE_NETRC_USERNAME)
  export GIT_NETRC_PASSEWORD := $(DRONE_NETRC_PASSWORD)
endif