export SEMVER := $(shell which semver)

ifneq ($(SEMVER),)
  ifeq (,$(filter semver,$(CI_STEPS_ADDITIONAL)))
    ifeq ($(DEBUG),true)
	  	$(warning enabling semver steps)
	  endif
	  export CI_STEPS_ADDITIONAL += semver

  endif

  ifneq (,$(filter semver,$(CI_STEPS_ADDITIONAL)))
    ifneq (,$(GIT_RELEASE))
      export SEMVER_CURRENT_VERSION := $(GIT_COMMIT_VERSION:$(GIT_TAG_VERSION_PREFIX)%=%)
      export SEMVER_RELEASE = $(GIT_RELEASE)
      export SEMVER_RELEASE_VERSION = $(GIT_RELEASE_VERSION:$(GIT_TAG_VERSION_PREFIX)%=%)
      export SEMVER_RELEASES = $(shell $(SEMVER) -p -r ">=$(SEMVER_RELEASE_VERSION)" $(GIT_RELEASE_VERSIONS) 2>/dev/null)
      export SEMVER_TYPE = $(GIT_RELEASE_TYPE)
      export SEMVER_DOCKER_BUILD ?= $(GIT_COMMIT_SHA_SHORT)

    endif

    export SEMVER_START_VERSION ?= 0.0.0
    ifneq (,$(SEMVER_TYPE))
      ifeq (,$(SEMVER_RELEASE_VERSION))
        export SEMVER_RELEASE_VERSION := $(SEMVER_START_VERSION)

      endif

      ifeq (,$(SEMVER_CURRENT_VERSION))
        export SEMVER_CURRENT_VERSION := $(SEMVER_START_VERSION)

      endif

      ifeq (,$(SEMVER_RELEASES))
        export SEMVER_RELEASES := $(SEMVER_START_VERSION)

      endif

      ifeq (release,$(SEMVER_TYPE))
        export SEMVER_INCREMENT = major
        export SEMVER_PREID = rc

      endif

      ifeq (feature,$(SEMVER_TYPE))
        export SEMVER_INCREMENT = minor
        export SEMVER_PREID = feature

      endif

      ifeq (hotfix,$(SEMVER_TYPE))
        export SEMVER_INCREMENT = patch
        export SEMVER_PREID = hotfix

      endif

      export SEMVER_NEXT_RELEASE_VERSION = $(shell $(SEMVER) -i $(SEMVER_INCREMENT) $(SEMVER_RELEASE_VERSION) 2>/dev/null)

      export SEMVER_INCREMENT_VERSION := $(shell $(SEMVER) -p -r ">=$(SEMVER_CURRENT_VERSION) <$(SEMVER_NEXT_RELEASE_VERSION)" $(SEMVER_RELEASES) | tail -n 1 2>/dev/null)

      ifneq ($(SEMVER_RELEASE_VERSION),$(SEMVER_INCREMENT_VERSION))
        export SEMVER_INCREMENT = release

      endif
  
      ifeq ($(SEMVER_RELEASE),production)
        export SEMVER_RELEASE_FLAGS := -i $(SEMVER_INCREMENT)

      else
        export SEMVER_RELEASE_FLAGS := -i pre$(SEMVER_INCREMENT) --preid $(SEMVER_PREID)

      endif

      export SEMVER_NEXT_VERSION = $(shell $(SEMVER) $(SEMVER_RELEASE_FLAGS) $(SEMVER_INCREMENT_VERSION))
    endif

    ifeq (,$(SEMVER_NEXT_VERSION))
      export SEMVER_NEXT_VERSION = $(SEMVER_CURRENT_VERSION)
    endif

    export SEMVER_GIT_TAG_VERSION := $(GIT_TAG_VERSION_PREFIX)$(SEMVER_NEXT_VERSION)$(SEMVER_GIT_TAG_BUILD:%=+%)

    ifeq (development,$(SEMVER_RELEASE))
      export SEMVER_DOCKER_TAGS := $(SEMVER_NEXT_VERSION)$(SEMVER_DOCKER_BUILD:%=-%)
    else
      ifeq (true,$(SEMVER_DOCKER_ALL))
        export SEMVER_VERSION_MAJOR := $(word 1,$(subst ., ,$(SEMVER_NEXT_VERSION)))
        export SEMVER_VERSION_MINOR := $(word 2,$(subst ., ,$(SEMVER_NEXT_VERSION)))

        export SEMVER_DOCKER_TAGS := $(SEMVER_VERSION_MAJOR)
        export SEMVER_DOCKER_TAGS += $(SEMVER_VERSION_MAJOR).$(SEMVER_VERSION_MINOR)
        export SEMVER_DOCKER_TAGS += $(SEMVER_NEXT_VERSION)

      else
        export SEMVER_DOCKER_TAGS := $(SEMVER_NEXT_VERSION)
      endif

      ifeq (true,$(SEMVER_DOCKER_LATEST))
        export SEMVER_DOCKER_TAGS += latest
      endif
    endif
  endif

else
  ifneq (,$(findstring semver,$(CI_STEPS_ADDITIONAL)))
## Initialize semver steps
init:: npm/install
	@exit 0

  endif
endif

ifeq (truesemver,$(CI)$(findstring semver,$(CI_STEPS_ADDITIONAL)))
cache:: 
	@$(SELF) -s cache/env \
  	CI_STEPS_CACHE_ENV="SEMVER_" \
  	CI_STEPS_CACHE="semver" \
		CI_STEPS_CACHE_EXT="mk" \
		CI_STEPS_CACHE_PREFIX="export " \
  	CI_STEPS_CACHE_SEP=" := "
	@[[ ! -z "$${SEMVER_DOCKER_TAGS}" ]] \
		&& echo "$${SEMVER_DOCKER_TAGS}" | tr " " "," > $(CI_WORKSPACE)/.tags \
		|| true

init::
	@$(SELF) -s cache/env/clear CI_STEPS_CACHE="semver"
endif
