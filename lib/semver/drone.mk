ifneq (,$(filter semver,$(CI_STEPS_ADDITIONAL)))
## (Optional) If no version found, what version to start with?
export SEMVER_START_VERSION ?= $(PLUGIN_START)
ifeq ($(SEMVER_START_VERSION),)
  export SEMVER_START_VERSION = 0.0.0
endif

## (Optional) Docker tags should include <major>, and <major>.<minor> as well as <major>.<minor>.<patch> for non-development releases?
export SEMVER_DOCKER_ALL ?= $(PLUGIN_DOCKER_ALL)

## (Optional) Add "latest" Docker tag to non-development release?
export SEMVER_DOCKER_LATEST ?= $(PLUGIN_DOCKER_LATEST)

endif